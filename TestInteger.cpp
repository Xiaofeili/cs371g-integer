// ---------------
// TestInteger.c++
// ---------------

// --------
// includes
// --------

#include "gtest/gtest.h"

#include "Integer.hpp"

using namespace std;
TEST(IntegerFixture, minus5) {
    Integer<char> x = -1998898814;
    x += 1374344043;
    ASSERT_EQ(x, -624554771);
}
TEST(IntegerFixture, multiply8) {
    Integer<char> x = 204820819;
    x *= 0;
    ASSERT_EQ(x, 0);
}




TEST(IntegerFixture, add10) {
    Integer<char> x = 99999;
    x += 99;
    ASSERT_EQ(x, 100098);
}




TEST(IntegerFixture, minus6) {
    Integer<char> x = 1000;
    x += -1;
    ASSERT_EQ(x, 999);
}

TEST(IntegerFixture,notEqual) {
    Integer<char> x = 100;
    Integer<char> y = 2;
    ASSERT_EQ(x != y, true);
}

TEST(IntegerFixture,lessEqual) {
    Integer<char> x = 100;
    Integer<char> y = 100;
    ASSERT_EQ(x <= y, true);
}

TEST(IntegerFixture,largeEqual) {
    Integer<char> x = 100;
    Integer<char> y = 100;
    ASSERT_EQ(x >= y, true);
}

TEST(IntegerFixture,plus) {
    Integer<char> x = 100;
    Integer<char> y = 100;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 200);
}

TEST(IntegerFixture, minus10) {
    Integer<char> x = 10;
    Integer<char> y = 1000;
    Integer<char> z = x- y;
    ASSERT_EQ(z, -990);
}

TEST(IntegerFixture, times) {
    Integer<char> x = 10;
    Integer<char> y = 1000;
    Integer<char> z = x * y;
    ASSERT_EQ(z, 10000);
}

TEST(IntegerFixture, shift) {
    Integer<char> x = -1234;
    x <<=  1;
    ASSERT_EQ(x, -12340);
}

TEST(IntegerFixture, shift2) {
    Integer<char> x = -1234;
    ASSERT_EQ(x >> 1, -123);
}


TEST(IntegerFixture, add8) {
    Integer<char> x = 1;
    x += 1000;
    ASSERT_EQ(x, 1001);
}

TEST(IntegerFixture, minus3) {
    Integer<char> x = 10;
    x -= 10000;
    ASSERT_EQ(x, -9990);
}

TEST(IntegerFixture, less_than0) {
    Integer<char> x = 1000;
    Integer<char> y = 999;
    ASSERT_EQ(false, x < y);
}

TEST(IntegerFixture, less_than1) {
    Integer<char> x = 998;
    Integer<char> y = 999;
    ASSERT_EQ(true, x < y);
}


TEST(IntegerFixture, less_than2) {
    Integer<char> x = 999;
    Integer<char> y = 999;
    ASSERT_EQ(false, x < y);
}

TEST(IntegerFixture, less_than3) {
    Integer<char> x = -1000;
    Integer<char> y = 999;
    ASSERT_EQ(true, x < y);
}



TEST(IntegerFixture, multiply) {
    Integer<char> x = 1000;
    x *= 0;
    ASSERT_EQ(x, 0);
}

TEST(IntegerFixture, multiply2) {
    Integer<char> x = 1000;
    x *= 1;
    ASSERT_EQ(x, 1000);
}
TEST(IntegerFixture, multiply3) {
    Integer<char> x = 99;
    x *= 88888;
    ASSERT_EQ(x, 8799912);
}
TEST(IntegerFixture, multiply4) {
    Integer<char> x = 55;
    x *= 78;
    ASSERT_EQ(x, 4290);
}

TEST(IntegerFixture, multiply5) {
    Integer<char> x = 1000;
    x *= -1;
    ASSERT_EQ(x, -1000);
}


TEST(IntegerFixture, multiply6) {
    Integer<char> x = 123456789;
    x *= 987654321;
    ASSERT_EQ(x, 121932631112635269);
}
TEST(IntegerFixture, multiply7) {
    Integer<char> x = 123456789987654321;
    x *= 987654321123456789;
    Integer<char> y("121932632103337905662094193112635269");
    ASSERT_EQ(x, y);
}

TEST(IntegerFixture, constructor0) {
    Integer<char> x = 100;
    ASSERT_EQ(x, 100);
}


TEST(IntegerFixture, constructor1) {
    string z = "100";
    Integer<char> y(z);
    ASSERT_EQ(100, y);
}

TEST(IntegerFixture, constructor2) {
    string z = "000100";
    Integer<char> x(z);
    ASSERT_EQ(x, 100);
}

TEST(IntegerFixture, ostream) {
    string z = "100";
    Integer<char> x(z);
    string y = "100";
    ostringstream oss;
    oss << x;
    ASSERT_EQ(oss.str(), y);
}


TEST(IntegerFixture, abs) {
    Integer<char> x = -100;
    ASSERT_EQ(x.abs(), 100);
}

TEST(IntegerFixture, abs2) {
    Integer<char> x = -100;
    ASSERT_EQ(abs(x), 100);
}

TEST(IntegerFixture, pow) {
    Integer<char> x = -100;
    ASSERT_EQ(x.pow(2), 10000);
}

TEST(IntegerFixture, pow2) {
    Integer<char> x = 123456789987654321;
    Integer<char> y("28679719750185954001002770474709371371037632314952243045355129195580567242895612575601");
    ASSERT_EQ(x.pow(5), y);
}

TEST(IntegerFixture, pow3) {
    Integer<char> x = -100;
    ASSERT_EQ(pow(x,2), 10000);
}

TEST(IntegerFixture, add) {
    Integer<char> x = 1001;
    x += 1;
    ASSERT_EQ(x, 1002);
}

TEST(IntegerFixture, add2) {
    Integer<char> x = 1001;
    x += -999;
    ASSERT_EQ(x, 2);
}

TEST(IntegerFixture, add3) {
    Integer<char> x = 56;
    x += -78;
    ASSERT_EQ(x, -22);
}

TEST(IntegerFixture, add4) {
    Integer<char> x = -1001;
    x += 1001;
    ASSERT_EQ(x, 0);
}

TEST(IntegerFixture, add5) {
    Integer<char> x = -999;
    x += 1001;
    ASSERT_EQ(x, 2);
}

TEST(IntegerFixture, add6) {
    Integer<char> x = -1;
    x ++;
    ASSERT_EQ(x, 0);
}

TEST(IntegerFixture, add7) {
    Integer<char> x = 9;
    x ++;
    ASSERT_EQ(x, 10);
}

TEST(IntegerFixture, minus) {
    Integer<char> x = 1001;
    x -= 1;
    ASSERT_EQ(x, 1000);
}

TEST(IntegerFixture, minus2) {
    Integer<char> x = 1000;
    x -= -999;
    ASSERT_EQ(x, 1999);
}


TEST(IntegerFixture, minus4) {
    Integer<char> x = 56;
    x -= 78;
    ASSERT_EQ(x, -22);
}



TEST(IntegerFixture, negate) {
    Integer<char> x = 100;
    Integer<char> y = -x;
    ASSERT_EQ(-100, y);
}


TEST(IntegerFixture, tostring0) {
    Integer<char> x = 100;
    string y = "100";
    ostringstream oss;
    oss << x;
    ASSERT_EQ(oss.str(), y);
}


TEST(IntegerFixture, tostring1) {
    Integer<char> x = -1000;
    string y = "-1000";
    ostringstream oss;
    oss << x;
    ASSERT_EQ(oss.str(),  y);
}

