// --------------
// RunInteger.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout, istream, ostream
#include <sstream>

#include "Integer.hpp"

using namespace std;

// ------------
// integer_read
// ------------
void integer_eval(istream& sin, ostream& sout) {
    string a;
    string b;
    string c;
    sin >> a >> b;
    Integer<char> ia(a);
    if(b == "+") {
        sin >> c;
        Integer<char> ic(c);
        ia += ic;
    } else if (b == "-") {
        sin >> c;
        Integer<char> ic(c);
        ia -= ic;
    } else if (b == "*") {
        sin >> c;
        Integer<char> ic(c);
        ia *= ic;
    } else if (b == "**") {
        int n;
        sin >> n;
        ia.pow(n);
    }
    sout << ia;
}
// ------------
// integer_eval
// ------------
void integer_read(istream& sin, ostream& sout) {
    string line;
    while(getline(sin, line)) {
        istringstream iss(line);
        integer_eval(iss, cout);
        sout << endl;
    }
}
// -------------
// integer_print
// -------------

// ----
// main
// ----

int main () {
    integer_read(cin, cout);

    return 0;
}
