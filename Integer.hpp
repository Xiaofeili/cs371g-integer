// ---------
// Integer.h
// ---------

#ifndef Integer_h
#define Integer_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <iostream>  // ostream
#include <stdexcept> // invalid_argument
#include <string>    // string
#include <vector>    // std::vector


const char PLUS = '0';
const char MINUS = '1';
const char ZERO = '0';
const char ONE = '1';
const int LIMIT = 9;

// -----------------
// shift_left_digits
// -----------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift left of the input sequence into the output sequence
 * ([b, e) << n) => x
 */
template <typename II, typename FI>
FI shift_left_digits (II b, II e, int n, FI x) {
    for(int i = 0 ; i < n; ++i)
        b++;
    e--;
    while(b != e) {
        *x = *b;
        x++;
        b++;
    }
    while(n) {
        *x = ZERO;
        x++;
        n--;
    }
    return x;
}

// ------------------
// shift_right_digits
// ------------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift right of the input sequence into the output sequence
 * ([b, e) >> n) => x
 */
template <typename II, typename FI>
FI shift_right_digits (II b, II e, int n, FI x) {
    while(n) {
        b++;
        n--;
    }
    while(b != e) {
        *x = *b;
        x++;
        b++;
    }
    return x;
}

// -----------
// plus_digits
// -----------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the sum of the two input sequences into the output sequence
 * ([b1, e1) + [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI plus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    int c = 0;
    int s = 0;
    while(b1 != e1 && b2 != e2) {
        s = (*b1 + *b2 + c - ZERO * 2);
        c = 0;
        assert(s >= 0 && s < 20);
        if(s >= 10) {
            s = s - 10 ;
            c = 1;
        }
        *x = s + ZERO;
        x++;
        b1++;
        b2++;
    }

    if(b1 == e1) {

        while(b2 != e2) {
            *x = *b2 + c;
            if(*x == ':') {
                *x = '0';
                c = 1;
            } else {
                c = 0;
            }
            b2++;
            x++;
        }
    } else {
        while(b1 != e1) {
            *x = *b1 + c;
            if(*x == ':') {
                *x = '0';
                c = 1;
            } else {
                c = 0;
            }
            b1++;
            x++;
        }
    }
    if(c == 1) {
        *x = ONE;
    }
    return x;
}

// ------------
// minus_digits
// ------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the difference of the two input sequences into the output sequence
 * ([b1, e1) - [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI minus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    int c = 0;
    int s = 0;
    while(b2 != e2 && b1 != e1) {
        int d1 = *b1 - ZERO - c;
        c = 0;
        int d2 = *b2 - ZERO;
        if(d1 < d2) {
            c = 1;
            d1 += 10;
        }
        s = d1 - d2;
        assert(s >= 0);
        *x = s + ZERO;
        x++;
        b1++;
        b2++;
    }
    while(b1 != e1) {
        *x = *b1 - c;
        if(*x == '/') {
            *x = '9';
            c = 1;
        } else {
            c = 0;
        }
        b1++;
        x++;
    }

    return x;
}




template <typename II>
std::vector<long long> blocks(II b1, II e1) {
    int count = 0;
    int factor = 1;
    std::vector<long long>  v;
    while(b1 != e1) {
        long long value = 0;
        while(b1 != e1 && count < LIMIT) {
            value += (*b1 - ZERO) * factor;
            factor *= 10;
            ++ count;
            b1++;
        }
        count = 0;
        factor = 1;
        v.push_back(value);
    }
    return v;
}


std::vector<char>  multiplies_long(long long value, int shift) {
    std::vector<char> result;
    int digit;
    for(int i = 0; i < shift * LIMIT; ++i) {
        result.push_back(ZERO);
    }
    while(value) {
        digit = value % 10;
        value = value / 10;
        result.push_back(ZERO + digit);
    }
    return result;
}

template <typename II>
void clear(II b1, II e1) {
    while(b1 != e1) {
        *b1 = ZERO;
        b1++;
    }
}

// -----------------
// multiplies_digits
// -----------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the product of the two input sequences into the output sequence
 * ([b1, e1) * [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI multiplies_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    std::vector<long long>  v1 = blocks(b1, e1);
    std::vector<long long>  v2 = blocks(b2, e2);
    clear(b1, e1);
    for(int i = 0; i < (int)v1.size(); ++i) {
        for(int j = 0; j < (int)v2.size(); ++j) {
            long long pr = v1[i] * v2[j];
            if(pr != 0) {
                std::vector<char> product = multiplies_long(pr, i + j);
                plus_digits(b1, e1, product.begin(), product.end(), b1);
            }
        }
    }
    return x;
}

// -------
// Integer
// -------

template <typename T, typename C = std::vector<T>>
class Integer {
    // -----------
    // operator ==
    // -----------

    /**
     * your documentation
     */
    friend bool operator == (const Integer& lhs, const Integer& rhs) {
        return lhs._x == rhs._x;
    }

    // -----------
    // operator !=
    // -----------

    /**
     * your documentation
     */
    friend bool operator != (const Integer& lhs, const Integer& rhs) {
        return !(lhs == rhs);
    }

    // ----------
    // operator <
    // ----------

    /**
     * your documentation
     */
    friend bool operator < (const Integer& lhs, const Integer& rhs) {
        auto lb = lhs._x.begin();
        auto rb = rhs._x.begin();
        bool flag = true;
        if(*lb == MINUS) {
            if(*rb == PLUS)
                return true;
            else
                flag = !flag;
        } else if (*rb == MINUS)
            return false;
        size_t llen = size(lhs);
        size_t rlen = size(rhs);
        lb++;
        rb++;
        if(llen > rlen)
            return !flag;
        else if (llen < rlen)
            return flag;
        else {
            for(size_t i = 1; i < llen; ++ i) {
                lb++;
                rb++;
            }
            for(size_t i = 0; i < llen; ++ i) {
                if(*lb < *rb) {
                    return flag;
                } else if (*rb < *lb) {
                    return !flag;
                }
                lb--;
                rb--;
            }
        }
        return false;
    }



    // -----------
    // operator <=
    // -----------

    /**
     * your documentation
     */
    friend bool operator <= (const Integer& lhs, const Integer& rhs) {
        return !(rhs < lhs);
    }

    // ----------
    // operator >
    // ----------

    /**
     * your documentation
     */
    friend bool operator > (const Integer& lhs, const Integer& rhs) {
        return (rhs < lhs);
    }

    // -----------
    // operator >=
    // -----------

    /**
     * your documentation
     */
    friend bool operator >= (const Integer& lhs, const Integer& rhs) {
        return !(lhs < rhs);
    }

    // ----------
    // operator +
    // ----------

    /**
     * your documentation
     */
    friend Integer operator + (Integer lhs, const Integer& rhs) {
        return lhs += rhs;
    }

    // ----------
    // operator -
    // ----------

    /**
     * your documentation
     */
    friend Integer operator - (Integer lhs, const Integer& rhs) {
        return lhs -= rhs;
    }

    // ----------
    // operator *
    // ----------

    /**
     * your documentation
     */
    friend Integer operator * (Integer lhs, const Integer& rhs) {
        return lhs *= rhs;
    }

    // -----------
    // operator <<
    // -----------

    /**
     * your documentation
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator << (Integer lhs, int rhs) {
        return lhs <<= rhs;
    }

    // -----------
    // operator >>
    // -----------

    /**
     * your documentation
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator >> (Integer lhs, int rhs) {
        return lhs >>= rhs;
    }

    // -----------
    // operator <<
    // -----------


    /**
     * your documentation
     */
    friend std::ostream& operator << (std::ostream& lhs, const Integer& rhs) {
        std::vector<char> v;
        auto b = rhs._x.begin();
        auto e = rhs._x.end();
        if(*b == MINUS)
            v.push_back('-');
        e--;
        while(e != b) {
            v.push_back(*e);
            e--;
        }
        std::string l(v.begin(), v.end());
        if (l.size() == 0)
            return lhs << "0";
        return lhs << l;
    }

    // ---
    // abs
    // ---

    /**
     * absolute value
     * your documentation
     */
    friend Integer abs (Integer x) {
        return x.abs();
    }

    // ---
    // pow
    // ---

    /**
     * power
     * your documentation
     * @throws invalid_argument if ((x == 0) && (e == 0)) || (e < 0)
     */
    friend Integer pow (Integer x, int e) {
        return x.pow(e);
    }


    friend size_t size(const Integer& x) {
        int count = 0;
        auto it = x._x.begin();
        auto e = x._x.end();
        it++;
        while(it != e) {
            if(*it == ZERO) {
                count++;
            } else {
                count = 0;
            }
            it++;
        }
        return x._x.size() - count - 1;
    }
private:
    // ----
    // data
    // ----

    C _x; // the backing container
    // <your data>

private:
    // -----
    // valid
    // -----

    bool valid () const { // class invariant
        auto b = _x.begin();
        auto e = _x.end();
        if(b == e)
            return true;
        char c = *b;
        if(c != PLUS && c != MINUS)
            return false;
        b++;
        while(b != e) {
            c = *b;
            if(c < ZERO || c > ZERO + 9)
                return false;
            b++;
        }
        return true;
    }

    void update() {
        int count = 0;
        auto it = _x.begin();
        auto e = _x.end();
        it++;
        while(it != e) {
            if(*it == ZERO) {
                count++;
            } else {
                count = 0;
            }
            it++;
        }
        if( count ) {
            for (int i = 0; i < count; ++i) {
                _x.pop_back();
            }
        }
        if(size(*this) == 0)
            *this = 0;
    }


public:
    // ------------
    // constructors
    // ------------

    /**
     * your documentation
     */
    Integer (long long value) {
        int digit;
        _x.reserve(96);
        if(value < 0) {
            _x.push_back(MINUS);
            value = - value;
        }

        else
            _x.push_back(PLUS);
        while(value) {
            digit = value % 10;
            value = value / 10;
            _x.push_back(ZERO + digit);
        }
        assert(valid());
    }

    /**
     * your documentation
     * @throws invalid_argument if value is not a valid representation of an Integer
     */
    explicit Integer (const std::string& value) {
        size_t len = value.size();
        int index = 0;
        _x.reserve(len + 2);
        if(value[0] == '-') {
            _x.push_back(MINUS);
            index++;
        } else
            _x.push_back(PLUS);
        if(value[0] == '+')
            index++;
        for(int i = len - 1; i >= index; --i) {
            char c = value[i];
            if(c < ZERO || c > ZERO + 9)
                throw std::invalid_argument("");
            else
                _x.push_back(c);
        }
        update();
    }

    Integer             (const Integer&) = default;
    ~Integer            ()               = default;
    Integer& operator = (const Integer&) = default;

    // ----------
    // operator -
    // ----------

    /**
     * your documentation
     */
    Integer operator - () const {
        Integer x = *this;
        auto it = x._x.begin();
        if(*it == PLUS)
            *it = MINUS;
        else
            *it = PLUS;
        return x;
    } // fix

    // -----------
    // operator ++
    // -----------

    /**
     * your documentation
     */
    Integer& operator ++ () {
        *this += 1;
        return *this;
    }

    /**
     * your documentation
     */
    Integer operator ++ (int) {
        Integer x = *this;
        ++(*this);
        return x;
    }

    // -----------
    // operator --
    // -----------

    /**
     * your documentation
     */
    Integer& operator -- () {
        *this -= 1;
        return *this;
    }

    /**
     * your documentation
     */
    Integer operator -- (int) {
        Integer x = *this;
        --(*this);
        return x;
    }

    // -----------
    // operator +=
    // -----------

    /**
     * your documentation
     */
    Integer& operator += (const Integer& rhs) {
        size_t len = size(rhs);
        for(size_t i =  size(*this); i < len; ++i)
            _x.push_back(ZERO);
        _x.push_back(ZERO);
        auto b1 = _x.begin();
        auto e1 = _x.end();
        auto b2 = rhs._x.begin();
        auto e2 = rhs._x.end();
        if(*b1 == *b2) {
            b1++;
            b2++;
            plus_digits(b1, e1, b2, e2, b1);
        } else {
            if(*b1 == MINUS) {
                if( abs() > rhs) {
                    *b1 = MINUS;
                    b1++;
                    b2++;
                    minus_digits(b1, e1, b2, e2, b1);
                } else {
                    b1++;
                    b2++;
                    minus_digits(b2, e2, b1, e1, b1);
                }
            } else {
                if( *this > -rhs) {

                    b1++;
                    b2++;
                    minus_digits(b1, e1, b2, e2, b1);
                } else {
                    *b1 = *b2;
                    b1++;
                    b2++;
                    minus_digits(b2, e2, b1, e1, b1);
                }
            }
        }
        update();

        return *this;
    }

    // -----------
    // operator -=
    // -----------

    /**
     * your documentation
     */
    Integer& operator -= (const Integer& rhs) {
        *this += -rhs;
        return *this;
    }

    // -----------
    // operator *=
    // -----------

    /**
     * your documentation
     */
    Integer& operator *= (const Integer& rhs) {
        if(rhs == 0) {
            *this = 0;
            return *this;
        }
        size_t len = size(*this);
        size_t newSize = len + size(rhs);
        for(size_t i = len; i < newSize; ++i)
            _x.push_back(ZERO);
        auto b1 = _x.begin();
        auto e1 = _x.end();
        auto b2 = rhs._x.begin();
        auto e2 = rhs._x.end();
        if(*b1 == *b2)
            *b1 = PLUS;
        else
            *b1 = MINUS;
        b1++;
        b2++;
        multiplies_digits(b1, e1, b2, e2, b1);
        update();
        return *this;
    }

    // ------------
    // operator <<=
    // ------------

    /**
     * your documentation
     */
    Integer& operator <<= (int n) {
        if(n < 0)
            return *this >>= (-n);
        for(int i = 0; i < n; ++i)
            _x.push_back(ZERO);
        auto b = _x.rbegin();
        auto e = _x.rend();
        shift_left_digits(b, e, n, b);
        return *this;
    }

    // ------------
    // operator >>=
    // ------------

    /**
     * your documentation
     */
    Integer& operator >>= (int n) {
        if(n < 0)
            return *this>>=(-n);
        auto b = _x.begin();
        auto e = _x.end();
        b++;
        shift_right_digits(b, e, n, b);
        for(int i = 0; i < n; ++i)
            _x.pop_back();
        return *this;
    }

    // ---
    // abs
    // ---

    /**
     * absolute value
     * your documentation
     */
    Integer& abs () {
        auto it = _x.begin();
        if(*it == MINUS)
            *it = PLUS;
        return *this;
    }

    // ---
    // pow
    // ---

    /**
     * power
     * your documentat
     ion
     * @throws invalid_argument if ((this == 0) && (e == 0)) or (e < 0)
     */
    Integer& pow (int e) {
        if(((int)size(*this) == 0) && (e == 0))
            throw std::invalid_argument("");
        if(e == 0) {
            *this = 1;
            return *this;
        }
        Integer x = *this;
        --e;
        while(e) {
            *this *= x;
            --e;
        }
        return *this;
    }
};



#endif // Integer_h
